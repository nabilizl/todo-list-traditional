podTemplate (
    yaml: '''
    apiVersion: v1
    kind: Pod
    spec:
      containers:
      - name: docker
        image: docker:dind
        securityContext:
          privileged: true
    ''',
    volumes: [
        hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
    ]){
    node(POD_LABEL) {
         env.NODEJS_HOME = "${tool 'Node'}"
         env.PATH="${env.NODEJS_HOME}/bin:${env.PATH}"
        try {
            stage("starting notifications") {
              notifyBuild("STARTED")
            }

            stage('Checkout') {
              echo "Create directory for source code"
              sh "mkdir code"
              dir('code') {
                checkout([$class: 'GitSCM', branches: [
                  [name: "develop"]
                ], userRemoteConfigs: [
                  [url: "https://gitlab.com/nabilizl/todo-list-express.git"]
                ]])
              }
            }

            stage('Code Review') {
              def scannerHome = tool "sonarscanner"
              withSonarQubeEnv("SonarQube") {
                dir('code') {
                  sh "${scannerHome}/bin/sonar-scanner"
                }
              }
              timeout(time: 5, unit: 'MINUTES') {
                def qg = waitForQualityGate()
                if (qg.status != 'OK') {
                  error "Pipeline aborted due to quality gate failure: ${qg.status}"
                }
              }
            }

            stage('Build') {
              dir('code') {
                container('docker') {
                  docker.withRegistry('', "docker_secret") {
                    def customImage = docker.build("inabiel/todo-express:latest")
                    customImage.push()
                  }
                }
              }
            }
              stage("Notification") {
                deleteDir()
                notifyBuild('SUCCESSFUL')
              }
            } catch (e) {
              stage('Error') {
                echo "${e}"
                deleteDir()
                currentBuild.result = 'FAILED'
                notifyBuild(currentBuild.result)
              }
          }
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"
 
  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary)
}
