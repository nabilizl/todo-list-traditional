const {PrismaClient} = require("@prisma/client");
const bcrypt = require("bcrypt");
const prisma = new PrismaClient();

let signIn = (req, res) => {
    return res.render("signin");
};

let signUp = (req, res) => {
    return res.render("signup");
};

let verifySignUp = async (req, res) => {
    try {
        var {uname, email, password} = req.body;

        const checkIfUserRegistered = await prisma.user.findUnique({
            where: {
                email,
            },
        });
        if (checkIfUserRegistered) {
            req.flash("error", "email sudah terdaftar!");
            return res.redirect("/auth/signup");
        }
        const hashedPassword = await bcrypt.hash(password, 10);
        const registeredUser = await prisma.user.create({
            data: {
                username: uname,
                email,
                password: hashedPassword,
            },
        });
        if (registeredUser) {
            return res.redirect("/auth/signin");;
        }
    } catch (e) {
        console.log(e);
    }
};

let verifySignIn = async (req, res) => {
    try {
        let {email, password} = req.body;
        const checkIfUserExists = await prisma.user.findUnique({
            where: {
                email,
            },
        });
        if (!checkIfUserExists) {
            req.flash("error", "user tidak terdaftar!");
            return res.redirect("/auth/signin");
        }
        const verifyPassword = await bcrypt.compare(
            password,
            checkIfUserExists.password
        );
        if (!verifyPassword) {
            req.flash("error", "password salah!");
            return res.redirect("/auth/signin");
        }
        req.session.data = {
            uid: checkIfUserExists.id,
            email: checkIfUserExists.email,
            uname: checkIfUserExists.username,
            isAuth: true,
        };
        return res.redirect("/home");
    } catch (e) {
        console.log(e);
    }
};



let logout = async (req, res) => {
    req.session = null;
    return res.redirect("/auth/signin");
};

module.exports = {signIn, signUp, verifySignUp, verifySignIn, logout};
