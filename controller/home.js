const {PrismaClient} = require("@prisma/client");
const {redirect} = require("express/lib/response");
const prisma = new PrismaClient();

const renderHome = async (req, res) => {
    try {
        const todos = await prisma.todo.findMany({
            orderBy: {
                'createdAt': 'asc'
            },
            where: {
                AND: [{
                    userId: req.session.data.uid,
                },
                {
                    isDone: false
                }
                ],
            },
            include: {
                user: {
                    select: {
                        id: true,
                        username: true
                    }
                }
            },
        })
        return res.render('index', {todos})
    }
    catch (e) {
        console.log(e)
    }
}

const renderEdit = async (req, res) => {
    try {
        let {id} = req.params
        const data = await prisma.todo.findFirst({
            where: {
                id: parseInt(id)
            }
        })
        return res.render('edit', {data})
    }
    catch (e) {
        console.log(e)
    }
}

const markAsDone = async (req, res) => {
    try {
        let {id} = req.params
        const getInitialData = await prisma.todo.findFirst({
            where: {
                id: parseInt(id)
            }
        })
        const markTodoAsDone = await prisma.todo.update({
            where: {
                id: parseInt(id)
            },
            data: {
                isDone: !getInitialData.isDone
            },
        })
        if (!markTodoAsDone) {
            req.flash('error', 'terjadi kesalahan')
            return res.redirect(`/home`)
        }
        req.flash('success', 'marked as Done!')
        return res.redirect(`/home`)
    }
    catch (e) {
        console.log(e)
    }
}

const updateTodoData = async (req, res) => {
    try {
        let {judul} = req.body
        let {id} = req.params
        const submitToDatabase = await prisma.todo.update({
            data: {
                title: judul
            },
            where: {
                id: parseInt(id)
            }
        })
        if (!submitToDatabase) {
            req.flash('error', 'terjadi kesalahan')
            return res.redirect(`${id}/edit`)
        }
        return res.redirect('/home')
    }
    catch (e) {
        console.log(e)
    }
}

const hapusData = async (req, res) => {
    try {
        let {id} = req.params
        var deleteFromDatabase = await prisma.todo.delete({
            where: {
                id: parseInt(id)
            }
        })
        if (!deleteFromDatabase) {
            req.flash('error', 'terjadi kesalahan')
            return res.redirect('/home')
        }
        return res.redirect('/home')
    } catch (e) {
        console.log(e)
    }

}

const submitTodo = async (req, res) => {
    try {
        let {judul, tanggal} = req.body
        const submitToDatabase = await prisma.todo.create({
            data: {
                title: judul,
                createdAt: new Date(tanggal),
                updatedAt: new Date(tanggal),
                userId: req.session?.data?.uid
            }
        })
        if (!submitToDatabase) {
            req.flash('error', 'terjadi kesalahan')
            return res.redirect('/home')
        }
        return res.redirect('/home')
    } catch (e) {
        console.log(e)
    }

}


module.exports = {renderHome, renderEdit, submitTodo, updateTodoData, hapusData, markAsDone}