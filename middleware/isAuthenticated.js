let checkLogin = (req, res, next) => {
    if (!req.session?.data?.isAuth) {
        req.flash('error', 'maaf, anda belum terautentikasi!')
        return res.redirect('/auth/signin')
    }
    next()
}
module.exports = {checkLogin}