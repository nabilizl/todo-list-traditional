let route = require("express").Router();
const {home} = require("nodemon/lib/utils");
let homeController = require("../controller/home");
const isAuthMiddleware = require('../middleware/isAuthenticated')

route.use(isAuthMiddleware.checkLogin)
route.get("/", homeController.renderHome);
route.get('/:id/mark', homeController.markAsDone)
route.get('/:id/edit', homeController.renderEdit)
route.post('/:id/delete', homeController.hapusData)
route.post('/:id/update', homeController.updateTodoData)
route.post('/create', homeController.submitTodo)
module.exports = route;
