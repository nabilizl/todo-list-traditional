let route = require("express").Router();
let authController = require("../controller/auth");

route.get("/signup", authController.signUp);
route.get("/signin", authController.signIn);
route.post("/signup", authController.verifySignUp);
route.post("/signin", authController.verifySignIn);
route.post('/logout', authController.logout)
module.exports = route;
