const Express = require("express");
const flash = require('express-flash');
const session = require('cookie-session')
const app = Express()
const expressLayouts = require('express-ejs-layouts')
const authRouter = require('./route/auth')
const homeRouter = require('./route/home')
const isAuthMiddleware = require('./middleware/isAuthenticated');

app.disable("x-powered-by");

app.use(session({
    name: "cookie-sess",
    keys: ["key-for-session"],
    path: '/',
    maxAge: 1000 * 60 * 60 * 24 * 7
}))
app.use(flash())

app.use(Express.urlencoded({extended: true}))
app.set('view engine', 'ejs')
app.use(expressLayouts)

app.use(function (req, res, next) {
    res.locals.isAuth = req.session?.data?.isAuth;
    next();
});


app.use(function (req, res, next) {
    res.locals.isAuth = req.session?.data?.isAuth;
    next();
});

app.get('/', isAuthMiddleware.checkLogin, (req, res) => {
    return res.redirect('/home')
})

app.use('/auth', authRouter)
app.use('/home', homeRouter)

app.listen(5000, () => {
    console.log(`Aplikasi telah berjalan pada port 5000 \n link : http://localhost:5000`)
})